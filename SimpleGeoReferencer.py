# -*- coding: utf-8 -*-
"""
/***************************************************************************
 SimpleGeoReferencer
                                 A QGIS plugin
                              -------------------
        begin                : 2019-08-24
        email                : kumahl@gmail.com
 ***************************************************************************/
""" 
import os.path

from qgis.core import *
from qgis.gui import *

from osgeo import gdal, osr
import processing

import numpy as np
import math

from PyQt5.QtCore import QSettings, QTranslator, qVersion, QCoreApplication, QSize, QPoint, QSizeF
from PyQt5.QtGui import QIcon, QPixmap, QPainter, QPen, QScreen, QColor, QKeySequence, QFont, QTextDocument
from PyQt5.QtWidgets import QAction, QFileDialog, QMessageBox, QLabel, QColorDialog, QWidget, QShortcut, QApplication, QTableWidgetItem, QHeaderView

# Initialize Qt resources from file resources.py
from .resources import *
# Import the code for the dialog
from .SimpleGeoReferencer_dialog import SimpleGeoReferencerDialog
from .decideGCP_dialog import decideGCPDialog
#import SimpleGeoReferencerGUI

tableWidget = ''
pos_tmp = ''
dlg2 = ''
pt = []
gpt = []
check = []


class SimpleGeoReferencer:
    """QGIS Plugin Implementation."""

    def __init__(self, iface):
        """Constructor.

        :param iface: An interface instance that will be passed to this class
            which provides the hook by which you can manipulate the QGIS
            application at run time.
        :type iface: QgsInterface
        """
        # Save reference to the QGIS interface
        self.iface = iface
        # initialize plugin directory
        self.plugin_dir = os.path.dirname(__file__)
        # initialize locale
        locale = QSettings().value('locale/userLocale')[0:2]
        locale_path = os.path.join(
            self.plugin_dir,
            'i18n',
            'SimpleGeoReferencer_{}.qm'.format(locale))

        if os.path.exists(locale_path):
            self.translator = QTranslator()
            self.translator.load(locale_path)

            if qVersion() > '4.3.3':
                QCoreApplication.installTranslator(self.translator)
        # Create the dialog (after translation) and keep reference
        self.dlg = SimpleGeoReferencerDialog()
        global dlg2
        dlg2 = decideGCPDialog()
        # Declare instance attributes
        self.actions = []
        self.menu = self.tr(u'&SimpleGeoReferencer')
        # TODO: We are going to let the user set this up in a future iteration
        self.toolbar = self.iface.addToolBar(u'SimpleGeoReferencer')
        self.toolbar.setObjectName(u'SimpleGeoReferencer')

        self.dlg.label_33 = MyLabel(self.dlg.scrollAreaWidgetContents)
        global tableWidget
        tableWidget = self.dlg.tableWidget


    # noinspection PyMethodMayBeStatic
    def tr(self, message):
        """Get the translation for a string using Qt translation API.

        We implement this ourselves since we do not inherit QObject.

        :param message: String for translation.
        :type message: str, QString

        :returns: Translated version of message.
        :rtype: QString
        """
        # noinspection PyTypeChecker,PyArgumentList,PyCallByClass
        return QCoreApplication.translate('SimpleGeoReferencer', message)


    def add_action(
        self,
        icon_path,
        text,
        callback,
        enabled_flag=True,
        add_to_menu=True,
        add_to_toolbar=True,
        status_tip=None,
        whats_this=None,
        parent=None):
        """Add a toolbar icon to the toolbar.

        :param icon_path: Path to the icon for this action. Can be a resource
            path (e.g. ':/plugins/foo/bar.png') or a normal file system path.
        :type icon_path: str

        :param text: Text that should be shown in menu items for this action.
        :type text: str

        :param callback: Function to be called when the action is triggered.
        :type callback: function

        :param enabled_flag: A flag indicating if the action should be enabled
            by default. Defaults to True.
        :type enabled_flag: bool

        :param add_to_menu: Flag indicating whether the action should also
            be added to the menu. Defaults to True.
        :type add_to_menu: bool

        :param add_to_toolbar: Flag indicating whether the action should also
            be added to the toolbar. Defaults to True.
        :type add_to_toolbar: bool

        :param status_tip: Optional text to show in a popup when mouse pointer
            hovers over the action.
        :type status_tip: str

        :param parent: Parent widget for the new action. Defaults None.
        :type parent: QWidget

        :param whats_this: Optional text to show in the status bar when the
            mouse pointer hovers over the action.

        :returns: The action that was created. Note that the action is also
            added to self.actions list.
        :rtype: QAction
        """

        icon = QIcon(icon_path)
        action = QAction(icon, text, parent)
        action.triggered.connect(callback)
        action.setEnabled(enabled_flag)

        if status_tip is not None:
            action.setStatusTip(status_tip)

        if whats_this is not None:
            action.setWhatsThis(whats_this)

        if add_to_toolbar:
            self.toolbar.addAction(action)

        if add_to_menu:
            self.iface.addPluginToVectorMenu(
                self.menu,
                action)

        self.actions.append(action)

        return action

    def initGui(self):
        """Create the menu entries and toolbar icons inside the QGIS GUI."""

        icon_path = ':/plugins/SimpleGeoReferencer/pencil.svg'
        self.add_action(
            icon_path,
            text=self.tr(u'SimpleGeoReferencer'),
            callback=self.run,
            parent=self.iface.mainWindow())

        self.dlg.pushButton.clicked.connect(self.decideInputImgPath)
        self.dlg.pushButton_2.clicked.connect(self.createWorldFile)
        self.dlg.pushButton_3.clicked.connect(self.rmRow)

        
        dlg2.pushButton.clicked.connect(self.getGroundPts)
        dlg2.accepted.connect(self.unsetM)
        #self.dlg.pushButton_3.clicked.connect(self.drawLine)
        #self.dlg.pushButton_4.clicked.connect(self.resizeImg)
        #self.dlg.pushButton_5.clicked.connect(self.decideOutputImgPath)
        #self.dlg.pushButton_6.clicked.connect(self.decidePointColor)
        #self.dlg.pushButton_7.clicked.connect(self.decideLineColor)
        #self.dlg.pushButton_8.clicked.connect(self.addSVG)
        #self.dlg.pushButton_9.clicked.connect(self.decideSVGSource)

        #self.shortcut = QShortcut(QKeySequence("Ctrl+Z"), self.dlg)
        #self.shortcut.activated.connect(self.undo)        

    def unload(self):
        """Removes the plugin menu item and icon from QGIS GUI."""
        for action in self.actions:
            self.iface.removePluginVectorMenu(
                self.tr(u'&SimpleGeoReferencer'),
                action)
            self.iface.removeToolBarIcon(action)
        # remove the toolbar
        del self.toolbar


    def run(self):
        """Run method that performs all the real work"""
        self.dlg.show()
        # Run the dialog event loop
        self.dlg.exec_()
    
    def unsetM(self):
      self.iface.mapCanvas().unsetMapTool( self.pointEmitter )      

    def undo(self):
      pass





    ########################################################
    #                                                      #
    # draw function                                        #
    #                                                      #
    ######################################################## 

    def rmRow(self):
      tableWidget.removeRow(tableWidget.currentRow())
      global pt
      global gpt
      pt = []
      gpt = []
        
      for i in range(tableWidget.rowCount()):
        pt.append( (float(tableWidget.item(i, 0).text()), float(tableWidget.item(i, 1).text())) )
        gpt.append( QgsPointXY(float(tableWidget.item(i, 2).text()), float(tableWidget.item(i, 3).text())) )


      vertex_items = [ i for i in self.iface.mapCanvas().scene().items() if issubclass(type(i), QgsVertexMarker)]
      annotation_items = [ i for i in self.iface.mapCanvas().scene().items() if issubclass(type(i), QgsMapCanvasAnnotationItem)]

      for ver in vertex_items:
        if ver in self.iface.mapCanvas().scene().items():
          self.iface.mapCanvas().scene().removeItem(ver)

      for ann in annotation_items:
        if ann in self.iface.mapCanvas().scene().items():
          self.iface.mapCanvas().scene().removeItem(ann)

      for i, pos_tmp in enumerate(gpt):
        #m.setCenter((pos_tmp[0], pos_tmp[1]))
        mm = QgsVertexMarker(self.iface.mapCanvas())        
        mm.setCenter(pos_tmp)
        mm.setColor(QColor(255, 0, 0, 100))
        mm.setPenWidth(3)
        mm.hide()
        mm.show()

        
        lbltext = QTextDocument(str(i+1))
        label = QgsTextAnnotation(self.iface.mapCanvas())
        label.setDocument(lbltext)

        label.setMapPosition(pos_tmp)
        #label.setMapPosition(QgsPointXY(0,0))
        label.setFrameSize(QSizeF(lbltext.size().width(),lbltext.size().height()))

        symbol = QgsMarkerSymbol()
        symbol.setColor(QColor("#f7fbff"))
        symbol.setOpacity(0.6)

        symbol2 = QgsFillSymbol()
        symbol2.setColor(QColor("#f7fbff"))
        symbol2.setOpacity(0.6)        
        #symbol.setStrokeStyle(Qt.PenStyle(Qt.NoPen))

        #label.setFrameBorderWidth(0)
        #label.setFrameColor(QColor("#ffffff"))
        #label.setFrameBackgroundColor(QColor("#ffffff"))
        label.setMarkerSymbol(symbol)
        label.setFillSymbol(symbol2)

        QgsMapCanvasAnnotationItem(label, self.iface.mapCanvas())

      self.dlg.label_33.update()
      self.iface.mapCanvas().refresh()

    def getGroundPts(self):
      if QgsProject.instance().count() == 0:
        dlg2.hide()
        self.dlg.hide()        
        QMessageBox.warning(self.iface.mainWindow(), u"警告", u"請加入參考圖層!", QMessageBox.Cancel)
      
      else:
        self.pointEmitter = QgsMapToolEmitPoint(self.iface.mapCanvas())
        self.pointEmitter.canvasClicked.connect(self.clickNow2)
        self.iface.mapCanvas().setMapTool( self.pointEmitter )
       

        #tool = PolygonTool(self.iface.mapCanvas())
        #self.iface.mapCanvas().setMapTool(tool)  

      #QMessageBox.warning(self.iface.mainWindow(), u"警告", str(event.x()), QMessageBox.Cancel)

    def clickNow2(self, event):
      dlg2.lineEdit.clear()
      dlg2.lineEdit_2.clear()
      dlg2.lineEdit.setText(str(event.x()))
      dlg2.lineEdit_2.setText(str(event.y()))

      vertex_items = [ i for i in self.iface.mapCanvas().scene().items() if issubclass(type(i), QgsVertexMarker)]
      annotation_items = [ i for i in self.iface.mapCanvas().scene().items() if issubclass(type(i), QgsMapCanvasAnnotationItem)]

      for ver in vertex_items:
        if ver in self.iface.mapCanvas().scene().items():
          self.iface.mapCanvas().scene().removeItem(ver)

      for ann in annotation_items:
        if ann in self.iface.mapCanvas().scene().items():
          self.iface.mapCanvas().scene().removeItem(ann)

      #items = self.iface.mapCanvas().items()

      m = QgsVertexMarker(self.iface.mapCanvas())

      m.setCenter(QgsPointXY(event.x(), event.y()))
      m.setColor(QColor(255, 0, 0, 100))
      m.setPenWidth(3)
      m.hide()
      m.show()

      lbltext = QTextDocument(str(len(gpt)+1))
      label = QgsTextAnnotation(self.iface.mapCanvas())
      label.setDocument(lbltext)

      label.setMapPosition(QgsPointXY(event.x(), event.y()))
      #label.setMapPosition(QgsPointXY(0,0))
      label.setFrameSize(QSizeF(lbltext.size().width(),lbltext.size().height()))

      symbol = QgsMarkerSymbol()
      symbol.setColor(QColor("#f7fbff"))
      symbol.setOpacity(0.6)

      symbol2 = QgsFillSymbol()
      symbol2.setColor(QColor("#f7fbff"))
      symbol2.setOpacity(0.6)        
      #symbol.setStrokeStyle(Qt.PenStyle(Qt.NoPen))

      #label.setFrameBorderWidth(0)
      #label.setFrameColor(QColor("#ffffff"))
      #label.setFrameBackgroundColor(QColor("#ffffff"))
      label.setMarkerSymbol(symbol)
      label.setFillSymbol(symbol2)

      QgsMapCanvasAnnotationItem(label, self.iface.mapCanvas())



      for i, pos_tmp in enumerate(gpt):
        #m.setCenter((pos_tmp[0], pos_tmp[1]))
        mm = QgsVertexMarker(self.iface.mapCanvas())        
        mm.setCenter(pos_tmp)
        mm.setColor(QColor(255, 0, 0, 100))
        mm.setPenWidth(3)
        mm.hide()
        mm.show()

        
        lbltext = QTextDocument(str(i+1))
        label = QgsTextAnnotation(self.iface.mapCanvas())
        label.setDocument(lbltext)

        label.setMapPosition(pos_tmp)
        #label.setMapPosition(QgsPointXY(0,0))
        label.setFrameSize(QSizeF(lbltext.size().width(),lbltext.size().height()))

        symbol = QgsMarkerSymbol()
        symbol.setColor(QColor("#f7fbff"))
        symbol.setOpacity(0.6)

        symbol2 = QgsFillSymbol()
        symbol2.setColor(QColor("#f7fbff"))
        symbol2.setOpacity(0.6)        
        #symbol.setStrokeStyle(Qt.PenStyle(Qt.NoPen))

        #label.setFrameBorderWidth(0)
        #label.setFrameColor(QColor("#ffffff"))
        #label.setFrameBackgroundColor(QColor("#ffffff"))
        label.setMarkerSymbol(symbol)
        label.setFillSymbol(symbol2)

        QgsMapCanvasAnnotationItem(label, self.iface.mapCanvas())
        

      self.iface.mapCanvas().refresh()


    def createWorldFile(self):
      #QMessageBox.warning(self.iface.mainWindow(), u"警告", 'create world file', QMessageBox.Cancel)
      if tableWidget.rowCount() <4:
        dlg2.hide()
        self.dlg.hide()
        QMessageBox.warning(self.iface.mainWindow(), u"警告", u"請至少輸入4個控制點", QMessageBox.Cancel)

      else:
        gcpList = []
        #QMessageBox.warning(self.iface.mainWindow(), u"警告", tableWidget.item(tableWidget.rowCount()-1, 2).text(), QMessageBox.Cancel)
        for i in range(tableWidget.rowCount()):
          gcpList.append( gdal.GCP(float(tableWidget.item(i, 2).text()), float(tableWidget.item(i, 3).text()), 0, float(tableWidget.item(i, 0).text()), float(tableWidget.item(i, 1).text())))

        geotransform = gdal.GCPsToGeoTransform( gcpList )
        outWld = self.dlg.lineEdit_6.text()[:-2] + self.dlg.lineEdit_6.text()[-1:] + 'w'
        fp = open(outWld, "a")
        fp.write(str(geotransform[1]))
        fp.write('\n')
        fp.write(str(geotransform[4]))
        fp.write('\n')
        fp.write(str(geotransform[2]))
        fp.write('\n')
        fp.write(str(geotransform[5]))
        fp.write('\n')
        fp.write(str(geotransform[0] + 0.5 * geotransform[1] + 0.5 * geotransform[2]))
        fp.write('\n')
        fp.write(str(geotransform[3] + 0.5 * geotransform[4] + 0.5 * geotransform[5]))
        fp.close()

        if (fp):
          rlayer = QgsRasterLayer(self.dlg.lineEdit_6.text(), self.dlg.lineEdit_6.text())
          #rlayer.setCrs( QgsCoordinateReferenceSystem(3826, QgsCoordinateReferenceSystem.EpsgCrsId) )
          QgsProject.instance().addMapLayer(rlayer) 
        else:
          QMessageBox.warning(self.iface.mainWindow(), u"警告", u"轉換失敗! :(", QMessageBox.Cancel)

    def decideInputImgPath(self):
      inImgName, _filter = QFileDialog.getOpenFileName(None, u"請選擇檔案", '', "Images(*.png *.jpg *.tif)")
   
      if inImgName != '':
        if inImgName[-3:] == 'png' or inImgName[-3:] == 'jpg' or inImgName[-3:] == 'tif' or inImgName[-3:] == 'PNG' or inImgName[-3:] == 'JPG' or inImgName[-3:] == 'TIF':
          self.dlg.lineEdit_6.clear()
          self.dlg.lineEdit_6.setText(str(inImgName))

          # load img metadata
          img = gdal.Open(self.dlg.lineEdit_6.text())

        else:
          msg = '不支援 .' + inImgName[-3:] + '副檔名之檔案'
          QMessageBox.warning(self.iface.mainWindow(), u"警告", msg, QMessageBox.Cancel)

        self.dlg.label_33.setGeometry(QtCore.QRect(0, 0, 501, 361))
        self.dlg.scrollArea.setWidget(self.dlg.label_33)
    
        global pixmap
        pixmap = QPixmap(self.dlg.lineEdit_6.text())
        self.dlg.label_33.setPixmap(pixmap)


class MyLabel(QLabel):
    def __init__(self, parent=None):
      super(MyLabel, self).__init__(parent=parent)
      self.setMouseTracking(False)
      dlg2.accepted.connect(self.insertToTableWidget)

    def paintEvent(self, e):
      super(MyLabel, self).paintEvent(e)
      #global pixmap        
      qp = QPainter(self)
      qp.drawPixmap(0,0,pixmap)
      qp.setFont(QFont("times",10))

      if len(pt) > 0:
        for i, pos_tmp in enumerate(pt):
          #pen = QPen(ptColor[i], ptSize[i])
          pen = QPen(QtCore.Qt.red, 3)
          qp.setPen(pen)
          qp.drawPoint(pos_tmp[0], pos_tmp[1])
          qp.drawText(pos_tmp[0]-5, pos_tmp[1]-2, str(i+1))

    def mousePressEvent(self,event):
      pass

    def mouseReleaseEvent(self,event):
      global pos_tmp
      pos_tmp = (event.x(), event.y())
      dlg2.show()

    #HELMERT  TRANSFORMATIONS
    def helm_trans(self, gcps): #gcps is numpy.array [x, y, Xmap, Ymap]
       n = len(gcps)
       xo, yo, Xo, Yo = 0.0, 0.0, 0.0, 0.0

       #JANEK calculate center of gravity 
       for i in range(n): 
           xo, yo, Xo, Yo = xo + gcps[i,0], yo + gcps[i,1], Xo + gcps[i,2], Yo + gcps[i,3]

       xo, yo, Xo, Yo = xo/n, yo/n, Xo/n, Yo/n

       del_x, del_y, del_X, del_Y = gcps[:,0] - xo, gcps[:,1] - yo, gcps[:,2] - Xo, gcps[:,3] - Yo

       #JANEK calculation of unknowns
       a_up, a_down, b_up, b_down= 0, 0, 0, 0 
       for i in range(n):
           a_up += del_x[i]*del_Y[i] - del_y[i]*del_X[i]
           a_down += del_x[i]*del_x[i] + del_y[i]*del_y[i]
           b_up += del_x[i]*del_X[i] + del_y[i]*del_Y[i]

       b_down = a_down
       a = a_up/a_down
       b= b_up/b_down
       c = yo*a - xo*b + Xo
       d = -xo*a - yo*b + Yo # a,b,c,d are transformation parameters X = c + b*x - a*y / Y = d + a*x + b*y 

       #JANEK calculate new coordinates for points based on transformation values
       Xi = (gcps[:,0] - xo)*b - (gcps[:,1] - yo)*a + Xo 
       Yi = (gcps[:,0] - xo)*a + (gcps[:,1] - yo)*b + Yo

       #JANEK compare calculated values to "clicked" ones
       V_X = Xi - gcps[:,2] 
       V_Y = Yi - gcps[:,3]
       V_XY = np.sqrt(V_X*V_X + V_Y*V_Y)

       V_XY_sum_sq, V_X_sum_sq, V_Y_sum_sq = 0, 0, 0
       for i in range(n):
           V_XY_sum_sq += V_XY[i]*V_XY[i]
           V_X_sum_sq += V_X[i]*V_X[i]
           V_Y_sum_sq += V_Y[i]*V_Y[i]

       mo = math.sqrt(V_XY_sum_sq/(n)) #avarage error
       mox = math.sqrt(V_X_sum_sq/(n)) #avarage x error
       moy = math.sqrt(V_Y_sum_sq/(n)) #avarage y error

       return V_X, V_Y, V_XY, mo, mox, moy, [a, b, c, d]

    def insertToTableWidget(self):

    ########################################################
    #                                                      #
    # add point to pt[]                                    #
    #                                                      #
    ######################################################## 
      if dlg2.lineEdit.text() != '' and dlg2.lineEdit_2.text() != '':

        header = tableWidget.horizontalHeader()
        header.setSectionResizeMode(QHeaderView.ResizeToContents)

        rowPosition = tableWidget.rowCount()
        tableWidget.insertRow(rowPosition)

        tableItem = QTableWidgetItem()
        #tableItem.setCheckState(QtCore.Qt.Checked)

        #tableWidget.setItem(tableWidget.rowCount()-1,0, tableItem)
        tableWidget.setItem(tableWidget.rowCount()-1,0, QTableWidgetItem(str(pos_tmp[0])))
        tableWidget.setItem(tableWidget.rowCount()-1,1, QTableWidgetItem(str(pos_tmp[1])))
        tableWidget.setItem(tableWidget.rowCount()-1,2, QTableWidgetItem(dlg2.lineEdit.text()))
        tableWidget.setItem(tableWidget.rowCount()-1,3, QTableWidgetItem(dlg2.lineEdit_2.text()))

        global pt
        global gpt
        pt = []
        gpt = []
        gcp = []
        
        for i in range(tableWidget.rowCount()):
          pt.append( (float(tableWidget.item(i, 0).text()), float(tableWidget.item(i, 1).text())) )
          gpt.append( QgsPointXY(float(tableWidget.item(i, 2).text()), float(tableWidget.item(i, 3).text())) )

        if tableWidget.rowCount() > 3:
          for i in range(tableWidget.rowCount()):
            gcp.append(np.array([pt[0], pt[1], gpt[0], gpt[1]]))

          tableWidget.setItem(tableWidget.rowCount()-1,4, QTableWidgetItem(str(self.helm_trans(gcp))))
          #tableWidget.setItem(tableWidget.rowCount()-1,5, QTableWidgetItem(str(self.helm_trans(gcp)[1])))
          #tableWidget.setItem(tableWidget.rowCount()-1,6, QTableWidgetItem(str(self.helm_trans(gcp)[2])))

        dlg2.lineEdit.clear()
        dlg2.lineEdit_2.clear()

        self.update()


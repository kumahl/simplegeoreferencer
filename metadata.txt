[general]
name=SimpleGeoReference
qgisMinimumVersion=3.0
description=SimpleGeoReference for QGIS 3.x
version=1.0
author=kumahl
email=kumahl@gmail.com

about=SimpleGeoReference for QGIS 3.x

tracker=
# End of mandatory metadata

# Recommended items:

# Uncomment the following line and add your changelog:
changelog= 1.0 Release for QGIS 3.0

# Tags are comma separated with spaces allowed
#tags=python,wkt

#homepage=
category=Plugins
#icon=icon.png

# experimental flag
experimental=False

# deprecated flag (applies to the whole plugin, not just a single version)
deprecated=False
